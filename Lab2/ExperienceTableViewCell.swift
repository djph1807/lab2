//
//  ExperienceTableViewCell.swift
//  Lab2
//
//  Created by Philip Djup on 2019-11-07.
//  Copyright © 2019 Philip Djup. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
   
    @IBOutlet weak var yearTableLabel: UILabel!
    @IBOutlet weak var titleTableLabel: UILabel!
    @IBOutlet weak var titleTableImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
