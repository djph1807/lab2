//
//  SkillsViewController.swift
//  Lab2
//
//  Created by Philip Djup on 2019-11-01.
//  Copyright © 2019 Philip Djup. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet var popupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    func moveDown(view: UIView){
        view.center.y += 250
    }
    
    func moveUp(view: UIView) {
        view.center.y -= 200
    }
    
    func viewPopup(view: UIView) {
        view.center.x = 200
        view.center.y = 200
        
    }
    
    
    @IBAction func tappedButtonAction(_ sender: UIButton) {
        let duration: Double = 2.0
        UIView.animate(withDuration: duration, animations: {
            self.moveDown(view: self.boxView)
        }){(finished) in
            if finished {
                UIView.animate(withDuration: duration, animations: {
                    self.moveUp(view: self.boxView)
                }){(finished) in
                    if finished {
                        UIView.animate(withDuration: duration, animations: {
                            self.moveDown(view: self.boxView)
                        })
                        UIView.animate(withDuration: 3, animations: {
                            self.viewPopup(view: self.popupView)
                            self.view.addSubview(self.popupView)
                        })
                    }
                }
            }
            
        }
        
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
 

}
