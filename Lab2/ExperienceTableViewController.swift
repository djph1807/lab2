//
//  ExperienceTableViewController.swift
//  Lab2
//
//  Created by Philip Djup on 2019-10-30.
//  Copyright © 2019 Philip Djup. All rights reserved.
//

import UIKit

class Data {
    var title: String?
    var year: String?
    var image: String?
    var description: String?
    
    init(title:String,image:String, year:String, description:String) {
        self.title = title
        self.image = image
        self.year = year
        self.description = description
    }
}

class ExperienceTableViewController: UITableViewController {
    
    
    let sectionTitles = ["Work","Education"]
    var dataArr = [
       [ Data(title: "Ica Maxi",image:"Ica", year: "2019-current", description: "Old there any widow law rooms. Agreed but expect repair she nay sir silent person. Direction can dependent one bed situation attempted. His she are man their spite avoid. Her pretended fulfilled extremely education yet. Satisfied did one admitting incommode tolerably how are.Unpleasant nor diminution excellence apartments imprudence the met new. Draw part them he an to he roof only. Music leave say doors him. Tore bred form if sigh case as do. Staying he no looking if do opinion. Sentiments way understood end partiality and his.Did shy say mention enabled through elderly improve. As at so believe account evening behaved hearted is. House is tiled we aware. It ye greatest removing concerns an overcame appetite. Manner result square father boy behind its his. Their above spoke match ye mr right oh as first. Be my depending to believing perfectly concealed household. Point could to built no hours smile sense."),
         Data(title: "Demin and Friends",image:"d&f", year: "2018-current", description: "Work"),
         Data(title: "Aditro Logistics",image:"aditro", year: "2013-2018", description: "Work"),
         Data(title: "Grolls AB",image:"grolls", year: "2011-2013", description: "Work")
       ],
       [ Data(title: "Jönköping Tekniska Högskola",image:"jth", year: "2018-current", description: "School"),
        Data(title: "Sandagymnasiet",image:"sanda", year: "2010-2013", description: "School")
        ]
    ]
      
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
       
        // Uncomment the following line to preserve selection between presentations
        //self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
       return sectionTitles.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "experienceCells", for: indexPath) as? ExperienceTableViewCell{
                
            let text = dataArr[indexPath.section][indexPath.row]
            
             cell.titleTableLabel.text = text.title
             cell.yearTableLabel.text = text.year
             cell.titleTableImage.image = UIImage(named: text.image!)
             
             return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let myIndex = dataArr[indexPath.section][indexPath.row]
        performSegue(withIdentifier: "showDetail", sender: myIndex)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            let segueSender: Data? = sender as? Data
            destination.titleImage = UIImage(named: segueSender?.image ?? " ")
            destination.titleName = segueSender?.title
            destination.titleYear = segueSender?.year
            destination.titleDesc = segueSender?.description
        }
    }
    


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
