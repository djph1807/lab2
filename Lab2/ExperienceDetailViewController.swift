//
//  ExperienceDetailViewController.swift
//  Lab2
//
//  Created by Philip Djup on 2019-10-31.
//  Copyright © 2019 Philip Djup. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
   @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    var titleImage: UIImage?
    var titleName: String?
    var titleDesc: String?
    var titleYear: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleImageView.image = titleImage
        nameLabel.text = titleName
        descLabel.text = titleDesc
        yearLabel.text = titleYear

        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Get the new view controller using segue.destination.
         Pass the selected object to the new view controller.
    }
    */

}
